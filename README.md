# fjord cam scripts
https://manpages.ubuntu.com/manpages/bionic/man1/fswebcam.1.html

### Logitech Brio
```
Available Controls        Current Value   Range
------------------        -------------   -----
Brightness                80 (31%)        0 - 255
Contrast                  150 (58%)       0 - 255
Saturation                120 (47%)       0 - 255
White Balance Temperature, Auto True            True | False
Gain                      0 (0%)          0 - 255
Power Line Frequency      60 Hz           Disabled | 50 Hz | 60 Hz
White Balance Temperature 4000 (36%)      2000 - 7500
Sharpness                 150 (58%)       0 - 255
Backlight Compensation    1               0 - 1
Exposure, Auto            Aperture Priority Mode Manual Mode | Aperture Priority Mode
Exposure (Absolute)       250 (12%)       3 - 2047
Exposure, Auto Priority   True            True | False
Pan (Absolute)            0 (50%)         -36000 - 36000
Tilt (Absolute)           0 (50%)         -36000 - 36000
Focus (absolute)          0 (0%)          0 - 255
Focus, Auto               True            True | False
Zoom, Absolute            100 (0%)        100 - 500
```

### Mokose Camera
```
Available Controls        Current Value   Range
------------------        -------------   -----
Brightness                64 (100%)       -64 - 64
Contrast                  50 (52%)        0 - 95
Saturation                100 (100%)      0 - 100
Hue                       -5 (42%)        -32 - 32
White Balance Temperature, Auto True            True | False
Gamma                     110 (5%)        100 - 300
Gain                      0               0 - 8
Power Line Frequency      50 Hz           Disabled | 50 Hz | 60 Hz
White Balance Temperature 4600 (48%)      2800 - 6500
Sharpness                 2               1 - 7
Exposure, Auto            Aperture Priority Mode Manual Mode | Aperture Priority Mode
Exposure (Absolute)       166 (7%)        3 - 2047
Focus (absolute)          0 (0%)          0 - 700
Focus, Auto               True            True | False
Zoom, Absolute            0 (0%)          0 - 60
```

### GoPro Hero 3 White
Makes a lot of use of this documentation: https://github.com/KonradIT/goprowifihack/blob/master/HERO3/WifiCommands.md
Python script that controls the GoPro via REST endpoints on the camera's build-in wifi AP.
One drawback to this method is that the GoPro API only works over the AP that the camera broadcasts, which is not very strong or fast. The result is that it takes a long time from captures to transfer for each image (around 20 seconds), and it will be difficult to capture images any faster.

#### Routes Used
- `http://10.5.5.9/bacpac/SH?t=goprohero&p=%01` - Activate shutter (take single image)
- `http://10.5.5.9:8080/gp/gpMediaList` - Retrieve media list from GoPro
- `http://10.5.5.9:8080/videos/DCIM/[DIRECTORY]/[IMAGENAME]` - Download image with path
- `http://10.5.5.9/camera/DA?t=goprohero` - Delete images on camera (so that SD card won't fill up)


### Webcam 
Bash script that controls the webcam with `fswebcam`, which should be one of the faster options.
Round-trip times are shorter, 5-6 seconds, and the connection is safer (direct cable as opposed to wireless). The 

## Notes
- rsync in batches?
    - --time-limit 30  // transfer for 30 minutes and then stop.
    - --remove-source-files  // remove files from beaglebone as they are transferred.
- Local mass storage might be a better option then trying to transfer these back to the cluster.
- do we need to take pictures all year? Only during bright months?


## Tests

| Camera | Resolution | Capture Time | Picture Size | Data Rate Sec | Data Rate Hr | Data per year |
|---|---|---|---|---|---|---|
|HZQDLN Webcam HD 1080P|1920x1080|~6s|~1MB|180KB/s|648MB/h|5.7TB/y|
|GroPro Hero3 White|2592×1944|~20s|~2MB|100KB/s|360MB/h|3.2TB/y|
|   |   |   |   |   |   |   |

# quick notes
### Mar 15
Working on reserver. Webcam takes a picture once every 1.77 seconds, 2034 images in one hour, 529MB. This *24 means we getting 12,696MB of data per day (12.7GB). With a 1000GB partition for photos, this gives us about 78 days of space.

`webcam_capture2.sh` - ACTUALLY takes a picture once per second, based on epoch. This is almost double the space usage, about 21.5GB per day, 46 days of storage in a 1TB partition.
