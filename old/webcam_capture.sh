#!/bin/bash
for (( ; ; ))
do
	sleep 1
	echo "Capturing single image at $(date)" >> webcam.log
	fswebcam -r 1920x1080 --skip 5 img/webcam-$(date +%s).jpg
done;
