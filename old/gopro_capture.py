import subprocess
import json
import requests
import time
from datetime import datetime



def runcmd(cmd, verbose = False, *args, **kwargs):
    process = subprocess.Popen(
        cmd,
        stdout = subprocess.PIPE,
        stderr = subprocess.PIPE,
        text = True,
        shell = True
    )
    std_out, std_err = process.communicate()
    return std_out

def take_single():
    # Take single Image
    runcmd('wget "http://10.5.5.9/bacpac/SH?t=goprohero&p=%01" -O /dev/null', verbose = True)

    time.sleep(5) # Wait for image to write

    # Get file list
    output = runcmd('curl "10.5.5.9:8080/gp/gpMediaList"', verbose = True)
    filelist = json.loads(output)
    d = filelist["media"][0]['d']
    f = filelist["media"][0]['fs'][-1]['n']

    image_url = "http://10.5.5.9:8080/videos/DCIM/" + d + "/" + f
    img_data = requests.get(image_url).content
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")

    print("Collected " + image_url + " (" + current_time + ")")
    with open("img/" + f, 'wb') as handler:
        handler.write(img_data)

    time.sleep(5) # Wait for image to transfer
    

    # Remove all Images from SD
    runcmd('wget "http://10.5.5.9/camera/DA?t=goprohero" -O /dev/null', verbose = True)

    time.sleep(5)
    

def main():
    count = 1
    for x in range(count):
        take_single()

main()
